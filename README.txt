﻿// $Id$

---NOTICE----> This is mostly just a place holder as the module is still being developed.

__________________________________________________________________
)O( Bookings Engine 
-----------------------
The purpose is to provide drupal with an native booking system that can be 
expanded/enhanced through the use of contrib modules.

__________________________________________________________________
)O( Features
-----------------------
    * Integrate with Ubercart or other ecommerce modules.
    * Support currency; whether it's monetary or points or whatever!
    * A MyBookings block so the user can track their bookings
    * Utilise the date api
	* Has its' own calendar display functions (we will need at least year,month,week,day views)
	* Comes bundled with a room booking system module
	* Providea hooks for other modules to utilise
	
__________________________________________________________________
)O( Install
-----------------------
To install, place the entire bookings folder into your modules directory.
Go to administer -> site building -> modules and enable the main bookings module
also activate any contributed modules you would like. (they have their own README.txt files)

Now go to administer -> content management -> bookings. Create a new
bookable object and edit its features.  Then test by trying to make a booking.

__________________________________________________________________
)O( Notes
-----------------------
In the interest of organization .. each of the contributed "add-on" modules will have
thier own README.txt & INSTALL.txt files within their sub-folder; also their own doc
sections within the bookings module administration pages

__________________________________________________________________
)O( Relevant Links
-----------------------
A comprehensive guide to using the bookings module is available at...
http://... 

Bookings Module User Fourms
http://...

Drupal Groups
http://groups.drupal.org/booking-systems

